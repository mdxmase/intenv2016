import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.RaspiPin;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.meta.AdaBoostM1;
import weka.core.DenseInstance;
import weka.core.Attribute;
import weka.core.Instances;
import weka.core.SerializationHelper;
import weka.classifiers.functions.LibSVM;
import java.io.FileInputStream;
import org.opencv.highgui.VideoCapture;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import java.util.ArrayList;

public class Predict {
	
	private Instances getInstance(Mat descriptors) {
        int numberAttributes = descriptors.rows();
        
        ArrayList<Attribute> attributes = new ArrayList<Attribute>();
        for (int att = 0; att < numberAttributes; att++) {
            attributes.add(new Attribute("Attribute" + att, att));
        }
        
        // Empty set of instances
        Instances instances = new Instances("Image", attributes, 0);
        
        double[] values = new double[numberAttributes];
        for (int row = 0; row < descriptors.rows(); row++) {
            values[row] = descriptors.get(row, 0)[0];
        }
        instances.add(new DenseInstance(0, values));
        instances.setClassIndex(instances.numAttributes() - 1);
        return instances;
    }
	
	public void run() {
		GpioController gpio = GpioFactory.getInstance();
 		GpioPinDigitalOutput motor1 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_04);
		GpioPinDigitalOutput motor2 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_05);
		GpioPinDigitalOutput motor3 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_06);

		motor1.low();
		motor2.low();
		motor3.low();

		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

		VideoCapture camera = new VideoCapture(0);
		if(!camera.isOpened()){
			System.out.println("Camera Error");
        } else {
            System.out.println("Camera OK");
        }

        // load weka model
		//AdaBoostM1 model;
		//NaiveBayes model;
		LibSVM model;
		try {
			//model = (AdaBoostM1) SerializationHelper.read(new FileInputStream("adaboost.model"));
			//model = (NaiveBayes) SerializationHelper.read(new FileInputStream("bayes.model"));
			model = (LibSVM) SerializationHelper.read(new FileInputStream("svm.model"));
		} catch (FileNotFoundException e1) {
			System.out.println("Weka model not found");
			return;
		} catch (Exception e1) {
			System.out.println("Error while loading the weka model");
			return;
		}

		int i = 0;
		while (true) {
			Mat image = new Mat();
        	camera.read(image);
        	i++;
			System.out.println("Frame " + i);
			
			// Define 3 regions
			Rect roi1 = new Rect(0,0,image.width()/3,image.height());
            Rect roi2 = new Rect(image.width()/3-1,0,image.width()/3,image.height());
            Rect roi3 = new Rect(2*image.width()/3-1,0,image.width()/3,image.height());
           
			// Crop image in 3 separate images
            Mat image1 = new Mat(image,roi1);
			Mat image2 = new Mat(image,roi2);
			Mat image3 = new Mat(image,roi3);
			
			// compute features
			Mat feature1 = Feature.computeFeature(image1);
			Mat feature2 = Feature.computeFeature(image2);
			Mat feature3 = Feature.computeFeature(image3);
			
			try {
				// predict probabilities
				double[] P1 = model.distributionForInstance(getInstance(feature1).firstInstance());
				double[] P2 = model.distributionForInstance(getInstance(feature2).firstInstance());
				double[] P3 = model.distributionForInstance(getInstance(feature3).firstInstance());
				
				if (P1[0] > 0.5) {
					System.out.println("Left not walkable");
					motor1.high();
				} else {
					motor1.low();
				}
				if (P2[0] > 0.5) {
					System.out.println("Forward not walkable");
					motor2.high();
                } else {
                	motor2.low();
                }
				if (P3[0] > 0.5) {
					System.out.println("Right not walkable");
					motor3.high();
				} else {
					motor3.low();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			image.release();
			image1.release();
			image2.release();
			image3.release();
			feature1.release();
			feature2.release();
			feature3.release();
		}	
	}

	public static void main (String[] args) {
		Predict fd = new Predict();
		fd.run();	
	}
}
