import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.highgui.Highgui;

public class Label {

	String pathCropped = "img/cropped/";
	String pathPos = "img/pos/";
	String pathNeg = "img/neg/";
	
	public void run() {

		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

		File folder = new File("img/originals");
		File[] listOfFiles = folder.listFiles();
		
		for (int i = 0; i < listOfFiles.length; i++){
			String filename = listOfFiles[i].getName();
			String extension = filename.substring(filename.lastIndexOf(".") + 1, filename.length());
			
			if (!(extension.equals("png") || extension.equals("jpg")))
				continue;
			
			System.out.println("reading: " + filename);

			Mat image = Highgui.imread(listOfFiles[i].toString());

			// Define 3 regions
			Rect roi1 = new Rect(0,0,image.width()/3,image.height());
            Rect roi2 = new Rect(image.width()/3-1,0,image.width()/3,image.height());
            Rect roi3 = new Rect(2*image.width()/3-1,0,image.width()/3,image.height());
	
			// Crop images in 3 separate images
            Mat image1 = new Mat(image,roi1);
			Mat image2 = new Mat(image,roi2);
			Mat image3 = new Mat(image,roi3);

			Highgui.imwrite(pathCropped + filename + "-crop1.png", image1);
			Highgui.imwrite(pathCropped + filename + "-crop2.png", image2);
			Highgui.imwrite(pathCropped + filename + "-crop3.png", image3);
			
			image.release();
			image1.release();
			image2.release();
			image3.release();
		}

		System.out.println("Cropped files created. Now calling the method to classify them manually");
	
		folder = new File("img/cropped");
		listOfFiles = folder.listFiles();

		for (int j = 0; j < listOfFiles.length; j++) {
			String filename = listOfFiles[j].getName();
			System.out.println("Processing file " + filename);
			selectAndSave(filename);			
		}		
	}

	private void selectAndSave(String fn) {

		String fileName = fn;
		String fullPath = pathCropped + fileName;
		File file = new File(fullPath);
		BufferedImage imageBla;
		try {
			imageBla = ImageIO.read(file);
	
			JLabel label = new JLabel(new ImageIcon(imageBla));
			JFrame f = new JFrame();
			f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			f.getContentPane().add(label);
			f.pack();
			f.setLocation(200,200);
			f.setVisible(true);
	
			int reply = JOptionPane.showConfirmDialog(null, "Is it a walkable place?", "Please answer this question", JOptionPane.YES_NO_OPTION);
	
			if (reply == JOptionPane.YES_OPTION) {
				File outputfile = new File(pathPos + fileName);
				ImageIO.write((BufferedImage) imageBla, "jpg", outputfile);
			}
			if (reply == JOptionPane.NO_OPTION) {
				File outputfile = new File(pathNeg + fileName);
				ImageIO.write((BufferedImage) imageBla, "jpg", outputfile);
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main (String[] args) {
		Label fd = new Label();
		fd.run();
	}
}