import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.HOGDescriptor;

public class Feature {
	
	static int width = 210;
	static int height = 120;
	static int cellSide = 30;
	static int blockSide = 3*cellSide;
	static int blockStrideSide = cellSide;
	static int nBins = 9;
	
	public static Mat computeFeature(Mat image) {
		// convert to grayscale
		Mat grayImage = new Mat();
		Imgproc.cvtColor(image, grayImage, Imgproc.COLOR_RGB2GRAY);
		
		// resize
		Mat smallGrayImage = new Mat();
		Imgproc.resize(grayImage, smallGrayImage, new Size(height, width));

		//HOG feature
		Size windowSize = smallGrayImage.size();
		Size cellSize = new Size(cellSide, cellSide);
		Size blockSize = new Size(blockSide, blockSide);
		Size blockStride = new Size(blockStrideSide, blockStrideSide);
		
		HOGDescriptor hog = new HOGDescriptor(windowSize, blockSize, blockStride, cellSize, nBins);
		MatOfFloat feature = new MatOfFloat();
		hog.compute(smallGrayImage, feature);
		return feature;
	}
	
	public static int size() {
		return nBins * ( blockSide/cellSide) 
                * (blockSide/cellSide) * ((width 
                - blockSide)/blockStrideSide + 1) 
                * ((height - blockSide)
                / blockStrideSide + 1);
	}
}
