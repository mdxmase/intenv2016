import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;

public class Preprocess {
	
	PrintWriter printwriter;
		
	private String matToCSVString(Mat m) {
		//Precondition: m is a matrix x*y*1 (gray image)
		Size size = m.size();
		StringBuilder stringBuilder = new StringBuilder();
		float[] d = new float[(int) (size.width * size.height)];
		m.get(0, 0, d);
		for (int x = 0; x < size.height; x++) {
			for (int y = 0; y < size.width; y++) {
				if (stringBuilder.length() > 0)
					stringBuilder.append(",");
				stringBuilder.append(d[(int) (y * size.height + x)]);
				}
		}
		return stringBuilder.toString();
	}
		
	public void generateAndPrint(String path, String type) throws FileNotFoundException, UnsupportedEncodingException {
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles();
		
		for (int i = 0; i < listOfFiles.length; i++) {
			String filename = listOfFiles[i].getName();
			String extension = filename.substring(filename.lastIndexOf(".") + 1, filename.length());
			
			if (!(extension.equals("png") || extension.equals("jpg")))
				continue;

			String picPathName = path + filename;
							
			Mat image = Highgui.imread(picPathName);

			printwriter.println(type + "," + matToCSVString(Feature.computeFeature(image)));
		}
	}
	
	public void run() throws FileNotFoundException, UnsupportedEncodingException {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		
		String pathPos = "img/pos/";
		String pathNeg = "img/neg/";
		
		printwriter = new PrintWriter("img/dataset.csv", "UTF-8");
		printwriter.print("Class");
		System.out.println(Feature.size());
		for (int i = 0; i < Feature.size(); i++) {
			printwriter.print(","+ i);
		}

		printwriter.println();
					
		generateAndPrint(pathNeg, "N");
		generateAndPrint(pathPos, "Y");
		printwriter.close();
	}
	

	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		Preprocess fd = new Preprocess();
		fd.run();
	}
}

